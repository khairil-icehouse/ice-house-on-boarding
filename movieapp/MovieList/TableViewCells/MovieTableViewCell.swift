//
//  MovieTableViewCell.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit
import AlamofireImage

final class MovieTableViewCell: BaseTableViewCell {

	// MARK: Private Properties

	@IBOutlet private weak var titleLabel: UILabel!
	@IBOutlet private weak var releaseDateLabel: UILabel!
	@IBOutlet private weak var posterImageView: UIImageView!
	@IBOutlet private weak var overviewLabel: UILabel!

	override func awakeFromNib() {
		super.awakeFromNib()

		posterImageView.backgroundColor = .pickledBluewood
	}

	// MARK: Public Methods

	func bindCellViewModel(viewModel: MovieCellViewModel) {

		titleLabel.text = viewModel.title
		releaseDateLabel.text = viewModel.releaseDate
		overviewLabel.text = viewModel.overview

		guard let url = viewModel.posterUrl else {
			posterImageView.image = nil
			return
		}
		posterImageView.af_setImage(withURL: url)
	}
}
