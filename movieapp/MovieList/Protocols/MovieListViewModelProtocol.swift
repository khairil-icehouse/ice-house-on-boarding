//
//  MovieListViewModelProtocol.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import RxSwift

protocol MovieListViewModelProtocol {

	var numberOfMovies: Int { get }
	var retrieveMovieListFailObservable: Observable<Error> { get }
	var retrieveMovieListSuccessObservable: Observable<Void> { get }

	func onMovieSelectedAt(index: Int)
	func onViewDidLoad()
	func getMovieCellViewModel(index: Int) -> MovieCellViewModel?
	func getMovieIdAt(index: Int) -> Int?

}
