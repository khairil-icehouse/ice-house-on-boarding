//
//  MovieListViewModel.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import RxSwift

final class MovieListViewModel: BaseViewModel {

	// MARK: - Private Properties

	fileprivate var movies = [Movie]()
	fileprivate var movieCellViewModels = [MovieCellViewModel]()
	fileprivate let movieNetworkModel: MovieNetworkModelProtocol
	fileprivate let _retrieveMovieListSuccessObservable = PublishSubject<Void>()
	fileprivate let _retrieveMovieListFailObservable = PublishSubject<Error>()
	fileprivate let _shouldOpenMovieDetail = PublishSubject<Int>()

	init(movieNetworkModel: MovieNetworkModelProtocol) {
		self.movieNetworkModel = movieNetworkModel
	}

	// MARK: - Private Methods

	fileprivate func retrieveMovieList() {

		movieNetworkModel.getMovieList(
			sort: .popularity(descending: true),
			year: 2018,
			page: 1
		).subscribe(onNext: { [weak self] list in

			self?.movies = list
			self?.movieCellViewModels = list.map { MovieCellViewModel(movie: $0) }
			self?._retrieveMovieListSuccessObservable.onNext(Void())
		}, onError: { [weak self] error in
			self?._retrieveMovieListFailObservable.onNext(error)
		})
		.addDisposableTo(disposeBag)
	}
}

// MARK: - MovieListViewModelProtocol

extension MovieListViewModel: MovieListViewModelProtocol {

	var retrieveMovieListFailObservable: Observable<Error> {
		return _retrieveMovieListFailObservable.asObservable()
	}

	var retrieveMovieListSuccessObservable: Observable<Void> {
		return _retrieveMovieListSuccessObservable.asObservable()
	}

	func onViewDidLoad() {
		retrieveMovieList()
	}

	var numberOfMovies: Int {
		return movieCellViewModels.count
	}

	func onMovieSelectedAt(index: Int) {

		guard index < movies.count, let id = movies[index].id else {
			return
		}

		_shouldOpenMovieDetail.onNext(id)
	}

	func getMovieCellViewModel(index: Int) -> MovieCellViewModel? {

		guard index < movieCellViewModels.count else {
			return nil
		}

		return movieCellViewModels[index]
	}

	func getMovieIdAt(index: Int) -> Int? {

		guard index < movies.count else {
			return nil
		}

		return movies[index].id
	}

}
