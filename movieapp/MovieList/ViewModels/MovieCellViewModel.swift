//
//  MovieCellViewModel.swift
//  movieapp
//
//  Created by Khairil Ushan on 02/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation

struct MovieCellViewModel {

	// MARK: Public Properties

	let title: String
	let releaseDate: String
	let overview: String
	let posterPath: String

	var posterUrl: URL? {
		guard !posterPath.isEmpty else {
			return nil
		}

		return URL(string: ApiConstants.getImageUrl(path: posterPath))
	}

}

extension MovieCellViewModel {

	init(movie: Movie) {

		self.title = movie.title ?? ""
		self.releaseDate = movie.releaseDate ?? ""
		self.overview = movie.overview ?? ""
		self.posterPath = movie.posterPath ?? ""
	}
}
