//
//  MovieListViewController.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxMoya

final class MovieListViewController: UIViewController {

	enum NavigationEvent {
		case showMovieDetail(id: Int)
	}

	// MARK: Public Properties

	var onNavigationEvent: ((NavigationEvent) -> Void)?

	// MARK: Private Properties

	@IBOutlet private weak var moviesTableView: UITableView!

	fileprivate var viewModel: MovieListViewModelProtocol?
	fileprivate var disposeBag = DisposeBag()

	convenience init(viewModel: MovieListViewModelProtocol) {
		self.init()

		self.viewModel = viewModel
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		configureTableView()
		configureNavigationBar()

		bindViewModels()
	}

	// MARK: Private methods

	private func configureNavigationBar() {
		navigationItem.title = "Movie List"
	}

	private func configureTableView() {

		moviesTableView.delegate = self
		moviesTableView.dataSource = self
		moviesTableView.tableFooterView = UIView()
		moviesTableView.estimatedRowHeight = 96
		moviesTableView.backgroundColor = .mirage
		moviesTableView.rowHeight = UITableViewAutomaticDimension
		moviesTableView.registerCellByNib(MovieTableViewCell.self)
	}

	private func bindViewModels() {

		guard let viewModel = viewModel else {
			return
		}

		viewModel.onViewDidLoad()
		bindRetrieveMovieListResponse(viewModel)
	}

	private func bindRetrieveMovieListResponse(_ viewModel: MovieListViewModelProtocol) {

		viewModel.retrieveMovieListSuccessObservable
			.subscribe(onNext: { [weak self] in
				self?.moviesTableView.reloadData()
			})
			.addDisposableTo(disposeBag)

		viewModel.retrieveMovieListFailObservable
			.subscribe(onNext: { [weak self] error in
				self?.showAlert(
					title: "Error",
					message: error.localizedDescription,
					positiveButtonTitle: "OK"
				)
			})
			.addDisposableTo(disposeBag)
	}

}

// MARK: - UITableViewDelegate

extension MovieListViewController: UITableViewDelegate {

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

		guard let movieId = viewModel?.getMovieIdAt(index: indexPath.row) else {
			return
		}

		onNavigationEvent?(.showMovieDetail(id: movieId))
	}

}

extension MovieListViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		if let numberOfMovies = viewModel?.numberOfMovies {
			return numberOfMovies
		} else {
			return 0
		}
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		let cell = tableView.dequeueCellByNib(for: indexPath) as MovieTableViewCell

		if let cellViewModel = viewModel?.getMovieCellViewModel(index: indexPath.row) {
			cell.bindCellViewModel(viewModel: cellViewModel)
		}

		return cell
	}
}
