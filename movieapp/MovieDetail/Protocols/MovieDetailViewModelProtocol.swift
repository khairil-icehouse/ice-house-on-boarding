//
//  MovieDetailViewModelProtocol.swift
//  movieapp
//
//  Created by Khairil Ushan on 03/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import RxSwift

protocol MovieDetailViewModelProtocol {

	var numberOfSections: Int { get }
	var pageTitle: Observable<String> { get }
	var retrieveMovieDetailSuccessObservable: Observable<Void> { get }
	var retrieveMovieDetailFailObservable: Observable<Error> { get }

	func getSectionTypeAt(index: Int) -> MovieDetailViewModel.MovieDetailSection?
	func getNumberOfRows(section: Int) -> Int
	func onViewDidLoad()

}
