//
//  MovieDetailHeaderCellViewModel.swift
//  movieapp
//
//  Created by Khairil Ushan on 03/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation

struct MovieDetailHeaderCellViewModel {

	let backdropPath: String
	let title: String
	let tagline: String
	let posterPath: String

	var backdropUrl: URL? {

		guard let url = URL(string: ApiConstants.getImageUrl(path: backdropPath)) else {
			return nil
		}
		return url
	}

	var posterUrl: URL? {

		guard let url = URL(string: ApiConstants.getImageUrl(path: posterPath)) else {
			return nil
		}
		return url
	}

}

extension MovieDetailHeaderCellViewModel {

	init(movie: Movie) {

		self.backdropPath = movie.backdropPath ?? ""
		self.posterPath = movie.posterPath ?? ""
		self.tagline = movie.tagline ?? ""
		self.title = movie.title ?? ""
	}
}
