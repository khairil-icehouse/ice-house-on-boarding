//
//  MovieDetailViewModel.swift
//  movieapp
//
//  Created by Khairil Ushan on 03/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit
import RxSwift

final class MovieDetailViewModel: BaseViewModel {

	enum MovieDetailSection {
		case header(cellViewModel: MovieDetailHeaderCellViewModel)
		case textTitle(text: String)
		case textContent(text: String)

		var estimatedHeight: CGFloat {

			switch self {

			case .header:
				return 200

			case .textTitle:
				return 40

			case .textContent:
				return 60
			}
		}

		var height: CGFloat {

			switch self {

			case .textContent, .textTitle:
				return UITableViewAutomaticDimension

			default:
				return estimatedHeight
			}
		}
	}

	// MARK: Private Properties

	private var movie: Movie?

	private let movieId: Int
	private let networkModel: MovieNetworkModelProtocol

	fileprivate var sections = [MovieDetailSection]()

	fileprivate let _retrieveMovieDetailSuccessObservable = PublishSubject<Void>()
	fileprivate let _retrieveMovieDetailFailObservable = PublishSubject<Error>()
	fileprivate let _pageTitle = PublishSubject<String>()

	init(networkModel: MovieNetworkModelProtocol, movieId: Int) {

		self.networkModel = networkModel
		self.movieId = movieId
	}

	// MARK: Private Methods

	fileprivate func retrieveMovieDetail() {

		networkModel.getMovieDetail(id: movieId)
			.subscribe(onNext: { [weak self] detail in

				self?.movie = detail

				if let title = detail.title {
					self?._pageTitle.onNext(title)
				}

				self?.createCellViewModels()
				self?._retrieveMovieDetailSuccessObservable.onNext(Void())
			}, onError: { [weak self] error in
				self?._retrieveMovieDetailFailObservable.onNext(error)
			})
			.disposed(by: disposeBag)
	}

	private func createCellViewModels() {

		guard let movie = movie else {
			return
		}

		sections.append(.header(cellViewModel: MovieDetailHeaderCellViewModel(movie: movie)))

		createOverviewCellIfAvailable(movie: movie)
		createGenresCellIfAvailable(movie: movie)
		createRuntimeCellIfAvailable(movie: movie)
		createReleaseDateCellIfAvailable(movie: movie)
		createProductionCompaniesCellIfAvailable(movie: movie)
		createBudgetCellIfAvailable(movie: movie)
		createRevenueCellIfAvailable(movie: movie)
		createLanguageTitlesCellIfAvailable(movie: movie)
	}

	private func createOverviewCellIfAvailable(movie: Movie) {

		guard let overview = movie.overview else {
			return
		}

		sections.append(.textTitle(text: "Overview"))
		sections.append(.textContent(text: overview))
	}

	private func createGenresCellIfAvailable(movie: Movie) {

		let genreTitles = movie.genres?.compactMap({ $0.name }).joined(separator: ", ")

		guard let genres = genreTitles, !genres.isEmpty else {
			return
		}

		sections.append(.textTitle(text: "Genres"))
		sections.append(.textContent(text: genres))
	}

	private func createRuntimeCellIfAvailable(movie: Movie) {

		guard let runtime = movie.runtime, runtime != 0 else {
			return
		}

		sections.append(.textTitle(text: "Duration"))
		sections.append(.textContent(text: "\(runtime) Minutes"))
	}

	private func createReleaseDateCellIfAvailable(movie: Movie) {

		guard let releaseDate = movie.releaseDate, !releaseDate.isEmpty else {
			return
		}

		sections.append(.textTitle(text: "Release Date"))
		sections.append(.textContent(text: releaseDate))
	}

	private func createProductionCompaniesCellIfAvailable(movie: Movie) {

		let companyTitles = movie.productCompanies?.compactMap { $0.name }.joined(separator: ", ")

		guard let companies = companyTitles, !companies.isEmpty else {
			return
		}

		sections.append(.textTitle(text: "Producton Companies"))
		sections.append(.textContent(text: companies))
	}

	private func createBudgetCellIfAvailable(movie: Movie) {

		guard let budget = movie.budget, budget != 0, let formattedBudget = budget.asCurrency() else {
			return
		}

		sections.append(.textTitle(text: "Production Budget"))
		sections.append(.textContent(text: formattedBudget))
	}

	private func createRevenueCellIfAvailable(movie: Movie) {

		guard let revenue = movie.revenue, revenue != 0, let formattedRevenue = revenue.asCurrency() else {
			return
		}

		sections.append(.textTitle(text: "Revenue"))
		sections.append(.textContent(text: formattedRevenue))
	}

	private func createLanguageTitlesCellIfAvailable(movie: Movie) {

		let languageTitles = movie.spokenLanguages?.compactMap { $0.name }.joined(separator: ", ")

		guard let languages = languageTitles, !languages.isEmpty else {
			return
		}

		sections.append(.textTitle(text: "Languages"))
		sections.append(.textContent(text: languages))
	}

}

extension MovieDetailViewModel: MovieDetailViewModelProtocol {

	var retrieveMovieDetailSuccessObservable: Observable<Void> {
		return _retrieveMovieDetailSuccessObservable.asObservable()
	}

	var retrieveMovieDetailFailObservable: Observable<Error> {
		return _retrieveMovieDetailFailObservable.asObservable()
	}

	var pageTitle: Observable<String> {
		return _pageTitle.asObservable().filter { !$0.isEmpty }
	}

	var numberOfSections: Int {
		return sections.count
	}

	func getNumberOfRows(section: Int) -> Int {

		guard section < sections.count else {
			return 0
		}

		return 1
	}

	func onViewDidLoad() {
		retrieveMovieDetail()
	}

	func getSectionTypeAt(index: Int) -> MovieDetailViewModel.MovieDetailSection? {

		guard index < sections.count else {
			return nil
		}

		return sections[index]
	}

}

extension MovieDetailViewModel.MovieDetailSection: Equatable {

	public static func ==(
		lhs: MovieDetailViewModel.MovieDetailSection,
		rhs: MovieDetailViewModel.MovieDetailSection
	) -> Bool {

		switch (lhs, rhs) {

		case (.header, .header):
			return true

		case (.textTitle(let lText), .textTitle(let rText)):
			return lText == rText

		case (.textContent(let lText), .textContent(let rText)):
			return lText == rText

		default:
			return false
		}
	}
}
