//
//  MovieDetailViewController.swift
//  movieapp
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit
import RxSwift

final class MovieDetailViewController: UIViewController {

	enum NavigationEvent {
		case backToMovieList
	}

	// MARK: Public Properties

	var onNavigationEvent: ((NavigationEvent) -> Void)?

	// MARK: Private Properties

	@IBOutlet private weak var movieTableView: UITableView!

	private let disposeBag = DisposeBag()

	fileprivate var viewModel: MovieDetailViewModelProtocol?

	convenience init(viewModel: MovieDetailViewModelProtocol) {
		self.init()

		self.viewModel = viewModel
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		configureTableView()
		configureNavigationBar()

		bindViewModels()
	}

	// MARK: - Private Methods

	private func bindViewModels() {

		guard let viewModel = viewModel else {
			return
		}

		viewModel.onViewDidLoad()

		bindPageTitle(viewModel)
		bindRetrieveMovieDetailResponse(viewModel)
	}

	private func bindPageTitle(_ viewModel: MovieDetailViewModelProtocol) {

		viewModel.pageTitle
			.subscribe(onNext: { [weak self] title in
				self?.navigationItem.title = title
			})
			.addDisposableTo(disposeBag)
	}

	private func bindRetrieveMovieDetailResponse(_ viewModel: MovieDetailViewModelProtocol) {

		viewModel.retrieveMovieDetailSuccessObservable
			.subscribe(onNext: { [weak self] in
				self?.movieTableView.reloadData()
			})
			.addDisposableTo(disposeBag)

		viewModel.retrieveMovieDetailFailObservable
			.subscribe(onNext: { [weak self] error in

				self?.showAlert(
					title: "Error",
					message: error.localizedDescription,
					positiveButtonTitle: "OK"
				)
			})
			.addDisposableTo(disposeBag)
	}

	private func configureTableView() {

		movieTableView.tableFooterView = UIView()
		movieTableView.separatorColor = .mirage
		movieTableView.backgroundColor = .mirage
		movieTableView.delegate = self
		movieTableView.dataSource = self
		movieTableView.registerCellByNib(MovieDetailHeaderTableViewCell.self)
		movieTableView.registerCellByNib(MovieDetailTextTitleTableViewCell.self)
		movieTableView.registerCellByNib(MovieDetailTextContentTableViewCell.self)
	}

	private func configureNavigationBar() {

		navigationItem.leftBarButtonItem = UIBarButtonItem(
			image: UIImage(named: "back_ico"),
			style: .plain,
			target: self,
			action: #selector(onBackButtonTapped)
		)
	}

	@objc private func onBackButtonTapped() {
		onNavigationEvent?(.backToMovieList)
	}

}

extension MovieDetailViewController: UITableViewDelegate {

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

		guard let sectionType = viewModel?.getSectionTypeAt(index: indexPath.section) else {
			return 0
		}

		return sectionType.height
	}

	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {

		guard let sectionType = viewModel?.getSectionTypeAt(index: indexPath.section) else {
			return 0
		}

		return sectionType.estimatedHeight
	}

}

extension MovieDetailViewController: UITableViewDataSource {

	func numberOfSections(in tableView: UITableView) -> Int {

		guard let sectionCount = viewModel?.numberOfSections else {
			return 0
		}

		return sectionCount
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		guard let rowCount = viewModel?.getNumberOfRows(section: section) else {
			return 0
		}

		return rowCount
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		guard let sectionType = viewModel?.getSectionTypeAt(index: indexPath.section) else {
			fatalError("Can't recognize section \(indexPath.section) and row \(indexPath.row)")
		}

		switch sectionType {

		case .header(let cellViewModel):
			let cell = tableView.dequeueCellByNib(for: indexPath) as MovieDetailHeaderTableViewCell
			cell.bindCellViewModel(cellViewModel: cellViewModel)
			return cell

		case .textTitle(let text):
			let cell = tableView.dequeueCellByNib(for: indexPath) as MovieDetailTextTitleTableViewCell
			cell.bindCellViewModel(cellViewModel: text)
			return cell

		case .textContent(let text):
			let cell = tableView.dequeueCellByNib(for: indexPath) as MovieDetailTextContentTableViewCell
			cell.bindCellViewModel(cellViewModel: text)
			return cell
		}
	}
}
