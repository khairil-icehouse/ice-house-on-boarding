//
//  MovieDetailTextTableViewCell.swift
//  movieapp
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

final class MovieDetailTextContentTableViewCell: BaseTableViewCell {

	// MARK: Private Properties

	@IBOutlet private weak var contentLabel: UILabel!

	// MARK: Public Methods

	func bindCellViewModel(cellViewModel: String) {
		contentLabel.text = cellViewModel
	}

}
