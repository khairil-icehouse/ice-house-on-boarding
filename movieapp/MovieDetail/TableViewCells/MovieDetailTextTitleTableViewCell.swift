//
//  MovieDetailTextTitleTableViewCell.swift
//  movieapp
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

final class MovieDetailTextTitleTableViewCell: BaseTableViewCell {

	// MARK: Private Properties

	@IBOutlet private weak var titleLabel: UILabel!

	// MARK: Public Properties

	func bindCellViewModel(cellViewModel: String) {
		titleLabel.text = cellViewModel
	}

}
