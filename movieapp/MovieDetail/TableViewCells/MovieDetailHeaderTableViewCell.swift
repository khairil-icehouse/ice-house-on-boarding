//
//  MovieDetailHeaderTableViewCell.swift
//  movieapp
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

final class MovieDetailHeaderTableViewCell: BaseTableViewCell {

	@IBOutlet private weak var backdropImageView: UIImageView!
	@IBOutlet private weak var posterImageView: UIImageView!
	@IBOutlet private weak var titleLabel: UILabel!
	@IBOutlet private weak var taglineLabel: UILabel!

	override func awakeFromNib() {
		super.awakeFromNib()

		backdropImageView.backgroundColor = .pickledBluewood
		posterImageView.backgroundColor = .pickledBluewood
	}

	func bindCellViewModel(cellViewModel: MovieDetailHeaderCellViewModel) {

		titleLabel.text = cellViewModel.title
		taglineLabel.text = cellViewModel.tagline

		if let url = cellViewModel.backdropUrl {
			backdropImageView.af_setImage(withURL: url)
		} else {
			backdropImageView.image = nil
		}

		guard let url = cellViewModel.posterUrl else {
			posterImageView.image = nil
			return
		}

		posterImageView.af_setImage(withURL: url)
	}
}
