//
//  AppDelegate.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit
import RxMoya

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	// MARK: Public Properties

    var window: UIWindow?

	// MARK: Private Properties

	private var rootFlowController: RootFlowController?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
    ) -> Bool {

		configureRootFlowController()

		rootFlowController?.createInitialScreen()

		return true
    }

	// MARK: - Private methods

	private func configureRootFlowController() {

		rootFlowController = RootFlowController()

		if let navBar = rootFlowController?.navigationController.navigationBar {
			configureNavigationBar(navBar)
		}

		window = UIWindow(frame: UIScreen.main.bounds)
		window?.rootViewController = rootFlowController?.navigationController
		window?.makeKeyAndVisible()
	}

	private func configureNavigationBar(_ navBar: UINavigationBar) {
		navBar.isTranslucent = false
		navBar.tintColor = .white
		navBar.barTintColor = .pickledBluewood
		navBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
		navBar.layer.masksToBounds = false
		navBar.layer.shadowColor = UIColor.black.cgColor
		navBar.layer.shadowOpacity = 0.7
		navBar.layer.shadowOffset = CGSize(width: 0, height: 2)
	}

}

