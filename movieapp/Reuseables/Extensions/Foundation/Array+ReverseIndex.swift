//
//  Array+ReverseIndex.swift
//  My Blue Bird
//
//  Created by Ricardo Pramana Suranta on 8/18/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation

extension Array {

	
	/// Traverse through the array from the latest element, and will return the index where the passed `predicate` returns `true`.
	func reverseIndex(where predicate: (Element) -> Bool) -> Index? {
		
		let end = self.endIndex.advanced(by: -1)
		let start = self.startIndex
		
		for index in stride(from: end, to: start, by: -1) {
			
			let element = self[index]
			
			if predicate(element) {
				return index
			}
		}
		
		let startElement = self[start]
		
		if predicate(startElement) {
			return self.startIndex
		} else {
			return nil
		}
	}
}

extension Array where Element: Equatable {
	
	/// Returns the index for the latest occurence of `element` in the array. Will return `nil` if not available.
	func reverseIndex(of element: Element) -> Index? {
		
		return reverseIndex(where: { (currentElement: Element) -> Bool in
			return currentElement == element
		})
	}
}
