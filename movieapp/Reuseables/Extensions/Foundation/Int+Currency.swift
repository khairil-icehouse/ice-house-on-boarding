//
//  Int+Currency.swift
//  movieapp
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation

extension Int {

	func asCurrency() -> String? {

		let formatter = NumberFormatter()
		formatter.locale = Locale.current
		formatter.numberStyle = .currency

		guard let formattedAmount = formatter.string(from: NSNumber(value: self)) else {
			return nil
		}

		return formattedAmount
	}

}
