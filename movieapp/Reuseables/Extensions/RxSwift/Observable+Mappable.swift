//
//  Observable+Mappable.swift
//  movieapp
//
//  Created by Khairil Ushan on 03/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift
import RxMoya
import Moya

extension ObservableType where Self.E == Moya.Response {

	func mapModel<T: Mappable>(_ type: T.Type) -> Observable<T> {

		return mapString().map { jsonString -> T in
			guard let model = Mapper<T>().map(JSONString: jsonString) else {
				fatalError("Fail to serialize json: \(jsonString)")
			}
			return model
		}
	}
}
