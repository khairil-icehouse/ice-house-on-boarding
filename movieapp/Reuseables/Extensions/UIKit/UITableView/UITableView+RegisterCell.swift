//
//  UITableView+RegisterCell.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

extension UITableView {

	func registerCellByNib<T: UITableViewCell>(_ type: T.Type) {
		register(T.nib, forCellReuseIdentifier: T.reuseIdentifier)
	}
}
