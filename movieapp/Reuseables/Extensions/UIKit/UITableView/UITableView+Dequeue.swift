//
//  UITableView+Dequeue.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

extension UITableView {

	func dequeueCellByNib<T: UITableViewCell>(for indexPath: IndexPath) -> T {

		guard let cell = dequeueReusableCell(
			withIdentifier: T.reuseIdentifier,
			for: indexPath
		) as? T else {
			fatalError("Dequeueing \(T.reuseIdentifier) is failed")
		}

		return cell
	}

}
