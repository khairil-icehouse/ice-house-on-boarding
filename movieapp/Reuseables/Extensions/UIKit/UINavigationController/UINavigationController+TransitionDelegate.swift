//
//  UINavigationController+TransitionDelegate.swift
//  My Blue Bird
//
//  Created by Nanda Julianda Akbar on 9/18/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController: UINavigationControllerDelegate {
	
	// MARK: - Public methods
	
	func push(viewController: UIViewController, animated: Bool, customTransition: Bool = false) {
		
		if (isViewControllerNeedToAnimatedTransitioning() && (customTransition)) {
			
			delegate = self
			
		} else {
			
			delegate = nil
			
		}
		
		pushViewController(viewController, animated: animated)
	}
	
	func pop(animated: Bool, customTransition: Bool = false) {
		
		if (isViewControllerNeedToAnimatedTransitioning() && (customTransition)) {
			
			delegate = self
			
		} else {
			
			delegate = nil
			
		}
		
		popViewController(animated: animated)
	}
	
	// MARK: - `UINavigationControllerDelegate` methods
	
	public func navigationController(_ navigationController: UINavigationController,
	                                 animationControllerFor operation: UINavigationControllerOperation,
	                                 from fromVC: UIViewController,
	                                 to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		
		if let transitionAnimator = transitionAnimator(for: fromVC),
			let transitionAnimation = transitionAnimator.createTransitionAnimation() {
			
			return ViewControllerTransitionAnimation(transitionAnimation: transitionAnimation)
		}
		
		return nil
	}
	
	// MARK: - Private methods
	
	private func lastViewController<T: UIViewController>(as viewController: T.Type) -> T? {
		
		guard let lastViewController = self.viewControllers.last as? T else {
			return nil
		}
		
		return lastViewController
	}
	
	private func isViewControllerNeedToAnimatedTransitioning() -> Bool {
		
		guard let lastViewController = self.viewControllers.last else {
			return false
		}
		
		return lastViewController is NavigationTransitionAnimator
	}
	
	private func transitionAnimator(for viewController: UIViewController?) -> NavigationTransitionAnimator? {
		return viewController as? NavigationTransitionAnimator
	}
	
}
