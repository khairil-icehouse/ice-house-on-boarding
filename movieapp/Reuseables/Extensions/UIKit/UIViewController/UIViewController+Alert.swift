//
//  UIViewController+Alert.swift
//  movieapp
//
//  Created by Khairil Ushan on 02/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

extension UIViewController {

	func showAlert(
		title: String,
		message: String,
		positiveButtonTitle: String,
		positiveButtonHandler: ((UIAlertAction) -> Void)? = nil,
		negativeButtonTitle: String? = nil,
		negativeButtonHandler: ((UIAlertAction) -> Void)? = nil,
		animated: Bool = true,
		completion: (() -> Void)? = nil
	) {
		let alertController = UIAlertController(
			title: title,
			message: message,
			preferredStyle: .alert
		)
		let positiveAction = UIAlertAction(
			title: positiveButtonTitle,
			style: .default,
			handler: positiveButtonHandler
		)
		alertController.addAction(positiveAction)
		if let title = negativeButtonTitle {
			let negativeAction = UIAlertAction(
				title: title,
				style: .cancel,
				handler: negativeButtonHandler
			)
			alertController.addAction(negativeAction)
		}
		present(alertController, animated: animated, completion: completion)
	}
}
