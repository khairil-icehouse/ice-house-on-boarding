//
//  UITableViewCell+UINib.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

extension UITableViewCell {

	static var nib: UINib {
		return UINib(nibName: reuseIdentifier, bundle: nil)
	}

}
