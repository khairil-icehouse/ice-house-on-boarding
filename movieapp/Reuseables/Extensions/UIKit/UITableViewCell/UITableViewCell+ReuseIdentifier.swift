//
//  UITableViewCell+ReuseIdentifier.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

extension UITableViewCell {

	static var reuseIdentifier: String {
		return String(describing: self)
	}

}
