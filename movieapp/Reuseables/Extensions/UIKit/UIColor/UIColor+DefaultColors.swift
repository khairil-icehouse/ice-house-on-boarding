//
//  UIColor+DefaultColors.swift
//  movieapp
//
//  Created by Khairil Ushan on 03/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

extension UIColor {

	static var toryBlue: UIColor {
		return UIColor(red: 13/255, green: 71/255, blue: 161/255, alpha: 1)
	}

	static var denim: UIColor {
		return UIColor(red: 21/255, green: 101/255, blue: 192/255, alpha: 1)
	}

	static var pickledBluewood: UIColor {
		return UIColor(red: 36/255, green: 52/255, blue: 71/255, alpha: 1)
	}

	static var mirage: UIColor {
		return UIColor(red: 20/255, green: 29/255, blue: 38/255, alpha: 1)
	}

}
