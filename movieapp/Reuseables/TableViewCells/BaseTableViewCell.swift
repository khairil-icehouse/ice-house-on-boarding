//
//  BaseTableViewCell.swift
//  movieapp
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

	override func awakeFromNib() {
		super.awakeFromNib()

		backgroundColor = .mirage
		selectionStyle = .none
	}
}
