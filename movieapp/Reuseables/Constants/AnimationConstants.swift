//
//  AnimationConstants.swift
//  My Blue Bird
//
//  Created by Iqbal Ansyori on 12/19/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation

final class AnimationConstants {
	
	static let activeBookingInfoExpandDuration = 0.4
	static let activeBookingFinalizeDelay = 2.0
	static let viewControllerPopDuration = 0.4
	
}
