//
//  AnimationClosures.swift
//  My Blue Bird
//
//  Created by Ricardo Pramana Suranta on 10/12/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation

/**
Collection of closures that can be used for `UIView.animate` execution.
*/
struct AnimationClosures {
	
	/**
	Closure that should be executed inside `UIView.animate`'s `animations` block.
	*/
	let progressClosure: (() -> ())?
	
	/**
	Closure that should be executed inside `UIView.animate`'s `completion` block.
	*/
	let completionClosure: (() -> ())?
	
	init(progressClosure: (() -> ())? = nil,
	     completionClosure: (() -> ())? = nil) {
		
		self.progressClosure = progressClosure
		self.completionClosure = completionClosure
	}
}
