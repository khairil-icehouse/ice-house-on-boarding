//
//  AnimationClosureParameter.swift
//  My Blue Bird
//
//  Created by Nanda Julianda Akbar on 13/10/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation
import UIKit

struct AnimationClosureParameter<Source: UIViewController, Target: UIViewController> {
	
	/**
	Holds the source viewcontroller.
	*/
	let sourceViewController: Source
	
	/**
	Holds the target viewcontroller.
	*/
	let targetViewController: Target
	
	/**
	Holds main container view when creating the transition animation.
	*/
	let containerView: UIView
	
}
