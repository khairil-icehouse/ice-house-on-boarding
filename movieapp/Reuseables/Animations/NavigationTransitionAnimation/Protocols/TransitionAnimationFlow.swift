//
//  TransitionAnimationFlow.swift
//  My Blue Bird
//
//  Created by Nanda Julianda Akbar on 26/10/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation

protocol TransitionAnimationFlow: class {
	
	associatedtype AnimationFlow: Equatable
	
	var transitionAnimation: TransitionAnimation? { get set }
	
	func setTransitionAnimation(for flow: AnimationFlow)
	
}
