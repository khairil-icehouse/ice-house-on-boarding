//
//  TransitionAnimation.swift
//  My Blue Bird
//
//  Created by Nanda Julianda Akbar on 26/10/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation
import UIKit

protocol TransitionAnimation: class {
	
	/**
	Total duration when performing transition animation.
	*/
	var transitionDuration: TimeInterval { get }
	
	/**
	Execute the transition animation process.
	*/
	func animate(using transitionContext: UIViewControllerContextTransitioning)
	
}

extension TransitionAnimation {
	
	func hideViews(_ views: [UIView]) {
		
		views.forEach({ (view: UIView) in
			view.isHidden = true
		})
	}
	
	func unhideViews(_ views: [UIView]) {
		
		views.forEach({ (view: UIView) in
			view.isHidden = false
		})
	}
	
	func addViews(_ views: [UIView], into containerView: UIView) {
		
		views.forEach({ (view: UIView) in
			containerView.addSubview(view)
		})
	}
	
	func removeSnapshotViews(_ views: [UIView]) {
		
		views.forEach({ (view: UIView) in
			view.removeFromSuperview()
		})
	}
	
	func setFinalViewFrame(for viewController: UIViewController,
	                       using transitionContext: UIViewControllerContextTransitioning) {
		
		viewController.view.frame = transitionContext.finalFrame(for: viewController)
		viewController.view.setNeedsLayout()
		viewController.view.layoutIfNeeded()
	}
	
}
