//
//  NavigationTransitionAnimator.swift
//  My Blue Bird
//
//  Created by Nanda Julianda Akbar on 9/18/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation
import UIKit

protocol NavigationTransitionAnimator: class {
	
	func createTransitionAnimation() -> TransitionAnimation?
	
}

extension NavigationTransitionAnimator where Self: UIViewController { }
