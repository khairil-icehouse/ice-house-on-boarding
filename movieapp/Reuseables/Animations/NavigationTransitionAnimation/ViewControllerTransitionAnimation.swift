//
//  ViewControllerTransitionAnimation.swift
//  My Blue Bird
//
//  Created by Nanda Julianda Akbar on 9/18/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation
import UIKit

final class ViewControllerTransitionAnimation: NSObject {
	
	fileprivate var transitionAnimation: TransitionAnimation
	
	init(transitionAnimation: TransitionAnimation) {
		
		self.transitionAnimation = transitionAnimation
	}
	
}

extension ViewControllerTransitionAnimation: UIViewControllerAnimatedTransitioning {
	
	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		
		return transitionAnimation.transitionDuration
	}
	
	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		
		transitionAnimation.animate(using: transitionContext)
	}
	
}
