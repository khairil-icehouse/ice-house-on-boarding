//
//  MovieProductionCompany.swift
//  movieapp
//
//  Created by Khairil Ushan on 03/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import ObjectMapper

struct MovieProductionCompany {

	var id: Int?
	var logoPath: String?
	var name: String?
	var originCountry: String?

}

extension MovieProductionCompany: Mappable {

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		self.id <- map["id"]
		self.logoPath <- map["logo_path"]
		self.name <- map["name"]
		self.originCountry <- map["origin_country"]
	}

}
