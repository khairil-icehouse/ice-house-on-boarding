//
//  MovieGenre.swift
//  movieapp
//
//  Created by Khairil Ushan on 03/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import ObjectMapper

struct MovieGenre {

	var id: Int?
	var name: String?
	
}

extension MovieGenre: Mappable {

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		self.id <- map["id"]
		self.name <- map["name"]
	}

}
