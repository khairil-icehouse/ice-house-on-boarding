//
//  MovieLanguage.swift
//  movieapp
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import ObjectMapper

struct MovieLangauge {

	var iso6391: String?
	var name: String?

}

extension MovieLangauge: Mappable {

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		self.iso6391 <- map["iso_639_1"]
		self.name <- map["name"]
	}

}
