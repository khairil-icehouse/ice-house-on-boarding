//
//  Movie.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import ObjectMapper

struct Movie {

	var id: Int?
	var title: String?
	var voteCount: Int?
	var voteAverage: Float?
	var popularity: Float?
	var posterPath: String?
	var language: String?
	var genreIds: [Int]?
	var backdropPath: String?
	var overview: String?
	var releaseDate: String?
	var genres: [MovieGenre]?
	var homepage: String?
	var productCompanies: [MovieProductionCompany]?
	var productCountries: [MovieProductionCountry]?
	var status: String?
	var tagline: String?
	var budget: Int?
	var revenue: Int?
	var spokenLanguages: [MovieLangauge]?
	var runtime: Int?

}

extension Movie: Mappable {

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		self.id <- map["id"]
		self.title <- map["title"]
		self.voteCount <- map["vote_count"]
		self.voteAverage <- map["vote_average"]
		self.popularity <- map["popularity"]
		self.posterPath <- map["poster_path"]
		self.language <- map["language"]
		self.genreIds <- map["genre_ids"]
		self.backdropPath <- map["backdrop_path"]
		self.overview <- map["overview"]
		self.releaseDate <- map["release_date"]
		self.genres <- map["genres"]
		self.homepage <- map["homepage"]
		self.productCompanies <- map["production_companies"]
		self.productCountries <- map["production_countries"]
		self.status <- map["status"]
		self.tagline <- map["tagline"]
		self.budget <- map["budget"]
		self.revenue <- map["revenue"]
		self.spokenLanguages <- map["spoken_languages"]
		self.runtime <- map["runtime"]
	}

}
