//
//  MovieList.swift
//  movieapp
//
//  Created by Khairil Ushan on 02/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import ObjectMapper

struct MovieList: Mappable {

	var page: Int?
	var totalResult: Int?
	var totalPages: Int?
	var result: [Movie]?

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		self.page <- map["page"]
		self.totalResult <- map["total_results"]
		self.totalPages <- map["total_pages"]
		self.result <- map["results"]
	}

}
