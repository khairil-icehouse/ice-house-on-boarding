//
//  MovieProductionCountry.swift
//  movieapp
//
//  Created by Khairil Ushan on 03/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import ObjectMapper

struct MovieProductionCountry {

	var iso31661: String?
	var name: String?

}

extension MovieProductionCountry: Mappable {

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		self.iso31661 <- map["iso_3166_1"]
		self.name <- map["name"]
	}

}
