//
//  BaseViewController.swift
//  movieapp
//
//  Created by Khairil Ushan on 01/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit
import RxSwift

class BaseViewController: UIViewController {

	let disposeBag = DisposeBag()

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = UIColor.mirage
	}

}
