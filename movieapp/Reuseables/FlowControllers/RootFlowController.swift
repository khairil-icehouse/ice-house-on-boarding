//
//  RootFlowController.swift
//  My Blue Bird
//
//  Created by Iqbal Ansyori on 8/23/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import Foundation
import UIKit

final class RootFlowController {
	
	let navigationController: UINavigationController
	
	private let movieRootViewController: MovieFlowController

	init(navigationController: UINavigationController = UINavigationController()) {
		
		self.navigationController = navigationController
		
		movieRootViewController = MovieFlowController(navigationController: navigationController)
	}
	
	// MARK: - Public Methods
	
	func createInitialScreen() {
		movieRootViewController.push(screen: .movieList)
	}

}
