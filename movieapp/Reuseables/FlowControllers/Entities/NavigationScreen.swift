//
//  Entities.swift
//  My Blue Bird
//
//  Created by Ricardo Pramana Suranta on 8/18/17.
//  Copyright © 2017 Blue Bird Group. All rights reserved.
//

import UIKit

/// A Struct for representing FlowController's custom screen and corresponding `UIViewController`.
struct NavigationScreen<T: Equatable> {
	let screen: T
	let viewController: UIViewController
}

extension NavigationScreen: Equatable {
	
	static func ==(lhs: NavigationScreen, rhs: NavigationScreen) -> Bool {
		
		let sameScreen = lhs.screen == rhs.screen
		let sameViewController = lhs.viewController === rhs.viewController
		
		return sameScreen && sameViewController
	}
}
