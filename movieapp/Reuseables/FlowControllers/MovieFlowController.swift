//
//  MovieFlowController.swift
//  movieapp
//
//  Created by Khairil Ushan on 05/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import UIKit
import RxMoya

final class MovieFlowController: FlowController {

	enum Screen {
		case movieList
		case movieDetail(id: Int)
	}

	enum NavigationEvent {
		case showMovieDetail(id: Int)
		case backToMovieList
	}

	// MARK: Public Properties

	var horizontalNavigationStack: [NavigationScreen<Screen>] = []
	var verticalNavigationStack: [NavigationScreen<Screen>] = []

	let navigationController: UINavigationController

	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}

	// MARK: Public Methods

	func viewControllerFor(screen: Screen) -> UIViewController {

		switch screen {

		case .movieDetail(let id):
			return createMovieDetailViewController(movieId: id)

		case .movieList:
			return createMovieListViewController()
		}
	}

	// MARK: Private Methods

	private func createMovieListViewController() -> MovieListViewController {

		let provider = RxMoyaProvider<MovieMoyaTarget>()
		let networkModel = MovieNetworkModel(provider: provider)
		let viewModel = MovieListViewModel(movieNetworkModel: networkModel)
		let controller = MovieListViewController(viewModel: viewModel)

		controller.onNavigationEvent = { [weak self] event in

			switch event {

			case .showMovieDetail(let id):
				self?.push(screen: .movieDetail(id: id))
			}
		}

		return controller
	}

	private func createMovieDetailViewController(movieId: Int) -> MovieDetailViewController {

		let provider = RxMoyaProvider<MovieMoyaTarget>()
		let networkModel = MovieNetworkModel(provider: provider)
		let viewModel = MovieDetailViewModel(networkModel: networkModel, movieId: movieId)
		let controller = MovieDetailViewController(viewModel: viewModel)

		controller.onNavigationEvent = { [weak self] event in

			switch event {

			case .backToMovieList:
				self?.pop()
			}
		}

		return controller
	}

}

extension MovieFlowController.Screen: Equatable {

	public static func ==(lhs: MovieFlowController.Screen, rhs: MovieFlowController.Screen) -> Bool {

		switch (lhs, rhs) {

		case (.movieList, .movieList):
			return true

		case (.movieDetail(let lId), .movieDetail(let rId)):
			return lId == rId

		default:
			return false
		}
	}
}
