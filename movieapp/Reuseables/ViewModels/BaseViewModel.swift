//
//  BaseViewModel.swift
//  movieapp
//
//  Created by Khairil Ushan on 02/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import RxSwift

class BaseViewModel {

	let disposeBag = DisposeBag()

}
