//
//  MovieNetworkModel.swift
//  movieapp
//
//  Created by Khairil Ushan on 02/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import RxSwift
import RxMoya

protocol MovieNetworkModelProtocol {

	func getMovieList(sort: SortType, year: Int, page: Int) -> Observable<[Movie]>
	func getMovieDetail(id: Int) -> Observable<Movie>
	func searchMovie(query: String, page: Int) -> Observable<[Movie]>

}

final class MovieNetworkModel {

	fileprivate let provider: RxMoyaProvider<MovieMoyaTarget>

	init(provider: RxMoyaProvider<MovieMoyaTarget>) {
		self.provider = provider
	}

	// MARK: - File-Private Methods

	fileprivate func mapToMovies(list: MovieList) -> [Movie] {

		guard let movies = list.result else {
			return []
		}
		return movies
	}

}

// MARK: - MovieNetworkModelProtocol

extension MovieNetworkModel: MovieNetworkModelProtocol {

	func getMovieList(sort: SortType, year: Int, page: Int) -> Observable<[Movie]> {

		return provider.request(.discover(sort: sort, year: year, page: page))
			.mapModel(MovieList.self)
			.map(mapToMovies)
	}

	func getMovieDetail(id: Int) -> Observable<Movie> {

		return provider.request(.movieDetail(id: id))
			.mapModel(Movie.self)
	}

	func searchMovie(query: String, page: Int) -> Observable<[Movie]> {

		return provider.request(.searchMovie(query: query, page: page))
			.mapModel(MovieList.self)
			.map(mapToMovies)
	}

}
