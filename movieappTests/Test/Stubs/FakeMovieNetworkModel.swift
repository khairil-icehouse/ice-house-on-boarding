//
//  FakeMovieNetworkModel.swift
//  movieappTests
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import RxSwift
import RxTest

final class FakeMovieNetworkModel: MovieNetworkModelProtocol {

	enum FakeError: Error {
		case movieListError
		case movieDetailError
		case searchMovieError
	}

	// MARK: - Public

	var shouldGiveError = false

	let fakeMovieList = [
		DataFactory.createMovie(),
		DataFactory.createMovie(),
		DataFactory.createMovie()
	]
	let fakeMovieDetail = DataFactory.createMovie()

	func getMovieList(sort: SortType, year: Int, page: Int) -> Observable<[Movie]> {

		guard !shouldGiveError else {
			return Observable.error(FakeError.movieListError)
		}
		return Observable.just(fakeMovieList)
	}

	func getMovieDetail(id: Int) -> Observable<Movie> {

		guard !shouldGiveError else {
			return Observable.error(FakeError.movieDetailError)
		}
		return Observable.just(fakeMovieDetail)
	}

	func searchMovie(query: String, page: Int) -> Observable<[Movie]> {

		guard !shouldGiveError else {
			return Observable.error(FakeError.movieListError)
		}
		return Observable.just(fakeMovieList)
	}

}
