//
//  BaseTestCase.swift
//  movieapp
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import XCTest
import RxSwift

class BaseTestCase: XCTestCase {

	// MARK: Public Properties

	let disposeBag = DisposeBag()

	// MARK: Public Methods

	func async(timeout: TimeInterval = 5, block: (_ expectation: XCTestExpectation) -> Void) {
		let expect = expectation(description: "expect")
		block(expect)
		waitForExpectations(timeout: timeout, handler: nil)
	}
}
