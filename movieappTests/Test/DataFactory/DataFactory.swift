//
//  DataFactory.swift
//  movieappTests
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation

struct DataFactory {

	static var randomString: String {
		return UUID().uuidString
	}

	static var randomInt: Int {
		return Int(arc4random_uniform(100))
	}

	static var randomBool: Bool {
		return Int(arc4random_uniform(1)) == 1
	}

	static var randomFloat: Float {
		return Float(arc4random()) / Float(UINT32_MAX)
	}

	static func createMovieGenre() -> MovieGenre {
		return MovieGenre(id: randomInt, name: randomString)
	}

	static func createMovieProductionCompany() -> MovieProductionCompany {

		return MovieProductionCompany(
			id: randomInt,
			logoPath: randomString,
			name: randomString,
			originCountry: randomString
		)
	}

	static func createMovieProductionCountry() -> MovieProductionCountry {
		return MovieProductionCountry(iso31661: randomString, name: randomString)
	}

	static func createSpokenLanguage() -> MovieLangauge {
		return MovieLangauge(iso6391: DataFactory.randomString, name: DataFactory.randomString)
	}

	static func createMovie() -> Movie {

		return Movie(
			id: DataFactory.randomInt,
			title: DataFactory.randomString,
			voteCount: DataFactory.randomInt,
			voteAverage: DataFactory.randomFloat,
			popularity: DataFactory.randomFloat,
			posterPath: DataFactory.randomString,
			language: DataFactory.randomString,
			genreIds: [
				DataFactory.randomInt,
				DataFactory.randomInt,
				DataFactory.randomInt
			],
			backdropPath: DataFactory.randomString,
			overview: DataFactory.randomString,
			releaseDate: DataFactory.randomString,
			genres: [
				DataFactory.createMovieGenre(),
				DataFactory.createMovieGenre(),
				DataFactory.createMovieGenre()
			],
			homepage: DataFactory.randomString,
			productCompanies: [
				DataFactory.createMovieProductionCompany(),
				DataFactory.createMovieProductionCompany(),
				DataFactory.createMovieProductionCompany()
			],
			productCountries: [
				DataFactory.createMovieProductionCountry(),
				DataFactory.createMovieProductionCountry(),
				DataFactory.createMovieProductionCountry()
			],
			status: DataFactory.randomString,
			tagline: DataFactory.randomString,
			budget: DataFactory.randomInt,
			revenue: DataFactory.randomInt,
			spokenLanguages: [
				createSpokenLanguage(),
				createSpokenLanguage(),
				createSpokenLanguage()
			],
			runtime: DataFactory.randomInt
		)
	}
}
