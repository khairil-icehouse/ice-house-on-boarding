//
//  MovieDetailViewModelTest.swift
//  movieapp
//
//  Created by Khairil Ushan on 08/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import XCTest

final class MovieDetailViewModelTest: BaseTestCase {

	// MARK: Private Properties

	private var networkModel: FakeMovieNetworkModel!
	private var viewModel: MovieDetailViewModel!
	private var movieId: Int!

	override func setUp() {

		networkModel = FakeMovieNetworkModel()
		movieId = DataFactory.randomInt
		viewModel = MovieDetailViewModel(networkModel: networkModel, movieId: movieId)
	}

	func testRetrieveMovieDetailResponseSuccess() {

		async { expect in

			self.viewModel.retrieveMovieDetailSuccessObservable
				.subscribe(onNext: {
					expect.fulfill()
				})
				.disposed(by: disposeBag)

			viewModel.onViewDidLoad()
		}
	}

	func testRetrieveMovieDetailResponseFail() {

		async { expect in

			self.viewModel.retrieveMovieDetailFailObservable
				.subscribe(onNext: { error in
					let expectationError = FakeMovieNetworkModel.FakeError.movieDetailError.localizedDescription
					XCTAssertEqual(error.localizedDescription, expectationError)
					expect.fulfill()
				})
				.disposed(by: disposeBag)

			viewModel.onViewDidLoad()
		}
	}

	func testNumberOfSections() {

		async { expect in

			self.viewModel.retrieveMovieDetailSuccessObservable
				.subscribe(onNext: {
					XCTAssertEqual(self.viewModel.numberOfSections, 17)
					expect.fulfill()
				})
				.disposed(by: disposeBag)

			viewModel.onViewDidLoad()
		}
	}

	func testNumberOfRowsForValidSection() {

		async { expect in

			self.viewModel.retrieveMovieDetailSuccessObservable
				.subscribe(onNext: {
					XCTAssertEqual(self.viewModel.getNumberOfRows(section: 1), 1)
					XCTAssertEqual(self.viewModel.getNumberOfRows(section: 5), 1)
					XCTAssertEqual(self.viewModel.getNumberOfRows(section: 10), 1)
					XCTAssertEqual(self.viewModel.getNumberOfRows(section: 15), 1)
					expect.fulfill()
				})
				.disposed(by: disposeBag)

			viewModel.onViewDidLoad()
		}
	}

	func testNumberOfRowsForInvalidSection() {

		async { expect in

			self.viewModel.retrieveMovieDetailSuccessObservable
				.subscribe(onNext: {
					XCTAssertEqual(self.viewModel.getNumberOfRows(section: 100), 0)
					expect.fulfill()
				})
				.disposed(by: disposeBag)

			viewModel.onViewDidLoad()
		}
	}

	func testOverviewSectionDisplayed() {

		async { expect in

			self.viewModel.retrieveMovieDetailSuccessObservable
				.subscribe(onNext: {
					let overviewSectionTitle = MovieDetailViewModel.MovieDetailSection.textTitle(text: "Overview")
					let overviewSectionContent = MovieDetailViewModel.MovieDetailSection.textContent(
						text: self.networkModel.fakeMovieDetail.overview!
					)
					XCTAssertEqual(self.viewModel.getSectionTypeAt(index: 1), overviewSectionTitle)
					XCTAssertEqual(self.viewModel.getSectionTypeAt(index: 2), overviewSectionContent)
					expect.fulfill()
				})
				.disposed(by: disposeBag)

			viewModel.onViewDidLoad()
		}
	}

}
