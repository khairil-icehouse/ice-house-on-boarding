//
//  MovieDetailHeaderCellViewModelTest.swift
//  movieappTests
//
//  Created by Khairil Ushan on 10/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import XCTest

final class MovieDetailHeaderCellViewModelTest: BaseTestCase {

	func testInitializerMapCorrectly() {

		let mockMovie = DataFactory.createMovie()
		let cellViewModel = MovieDetailHeaderCellViewModel(movie: mockMovie)

		XCTAssertEqual(mockMovie.title, cellViewModel.title)
		XCTAssertEqual(mockMovie.backdropPath, cellViewModel.backdropPath)
		XCTAssertEqual(mockMovie.posterPath, cellViewModel.posterPath)
		XCTAssertEqual(mockMovie.tagline, cellViewModel.tagline)
	}

	func testBackDropUrlIsCorrect() {

		let mockMovie = DataFactory.createMovie()
		let cellViewModel = MovieDetailHeaderCellViewModel(movie: mockMovie)
		let expectBackDropUrl = "https://image.tmdb.org/t/p/w300\(cellViewModel.backdropPath)"

		XCTAssertEqual(cellViewModel.backdropUrl?.absoluteString, expectBackDropUrl)
	}

	func testPosterUrlIsCorrect() {

		let mockMovie = DataFactory.createMovie()
		let cellViewModel = MovieDetailHeaderCellViewModel(movie: mockMovie)
		let expectPosterUrl = "https://image.tmdb.org/t/p/w300\(cellViewModel.posterPath)"

		XCTAssertEqual(cellViewModel.posterUrl?.absoluteString, expectPosterUrl)
	}
}
