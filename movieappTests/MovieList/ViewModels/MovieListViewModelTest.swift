//
//  MovieListViewModelTest.swift
//  movieappTests
//
//  Created by Khairil Ushan on 04/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import Foundation
import XCTest
import RxSwift

@testable import movieapp

final class MovieListViewModelTest: BaseTestCase {

	// MARK: Private Properties

	private var networkModel: FakeMovieNetworkModel!
	private var viewModel: MovieListViewModel!

	override func setUp() {

		networkModel = FakeMovieNetworkModel()
		viewModel = MovieListViewModel(movieNetworkModel: networkModel)
	}

	// MARK: Public Methods

	func testRetrieveMovieListSuccess() {

		async { expect in

			self.viewModel.retrieveMovieListSuccessObservable
				.subscribe(onNext: {
					expect.fulfill()
				})
				.disposed(by: self.disposeBag)

			self.viewModel.onViewDidLoad()
		}
	}

	func testRetrieveMovieListFail() {

		networkModel?.shouldGiveError = true

		async { expect in

			self.viewModel.retrieveMovieListFailObservable
				.subscribe(onNext: { error in
					let fakeDescription = FakeMovieNetworkModel.FakeError.movieListError.localizedDescription
					XCTAssertEqual(error.localizedDescription, fakeDescription)
					expect.fulfill()
				})
				.disposed(by: self.disposeBag)

			self.viewModel.onViewDidLoad()
		}
	}

	func testNumberOfRows() {

		async { expect in

			self.viewModel.retrieveMovieListSuccessObservable
				.subscribe(onNext: { error in
					XCTAssertEqual(self.networkModel.fakeMovieList.count, self.viewModel.numberOfMovies)
					expect.fulfill()
				})
				.disposed(by: self.disposeBag)

			self.viewModel.onViewDidLoad()
		}
	}

	func testMovieCellViewModelByRowIndex() {

		async { expect in

			self.viewModel.retrieveMovieListSuccessObservable
				.subscribe(onNext: { error in
					let lastIndex = self.viewModel.numberOfMovies - 1
					let entity = self.networkModel.fakeMovieList[lastIndex]
					let cellViewModel = self.viewModel.getMovieCellViewModel(index: lastIndex)
					XCTAssertEqual(cellViewModel?.title, entity.title)
					XCTAssertEqual(cellViewModel?.overview, entity.overview)
					XCTAssertEqual(cellViewModel?.posterPath, entity.posterPath)
					XCTAssertEqual(cellViewModel?.releaseDate, entity.releaseDate)
					expect.fulfill()
				})
				.disposed(by: self.disposeBag)

			self.viewModel.onViewDidLoad()
		}
	}
	
}

