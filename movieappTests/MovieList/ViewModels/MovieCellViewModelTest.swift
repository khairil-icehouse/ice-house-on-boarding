//
//  MovieCellViewModelTest.swift
//  movieappTests
//
//  Created by Khairil Ushan on 05/10/18.
//  Copyright © 2018 Ice House, Inc. All rights reserved.
//

import XCTest

final class MovieCellViewModelTest: BaseTestCase {

	func testInitializerMapCorrectly() {

		let mockMovie = DataFactory.createMovie()
		let cellViewModel = MovieCellViewModel(movie: mockMovie)

		XCTAssertEqual(mockMovie.title, cellViewModel.title)
		XCTAssertEqual(mockMovie.releaseDate, cellViewModel.releaseDate)
		XCTAssertEqual(mockMovie.overview, cellViewModel.overview)
		XCTAssertEqual(mockMovie.posterPath, cellViewModel.posterPath)
	}

	func testPosterPathMapToCompleteUrl() {

		let mockMovie = DataFactory.createMovie()
		let cellViewModel = MovieCellViewModel(movie: mockMovie)
		let expectPosterUrl = "https://image.tmdb.org/t/p/w300\(cellViewModel.posterPath)"

		XCTAssertEqual(cellViewModel.posterUrl?.absoluteString, expectPosterUrl)
	}
}
